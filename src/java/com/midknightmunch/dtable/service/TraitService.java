/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.Trait;
import java.util.List;

/**
 *
 * @author KODE
 */
public interface TraitService {
    public Trait create(Trait t);
    public Trait get(Integer id);
    public List<Trait> getAll();
    public List<Trait> getByBackground(Integer backgroundId);
}
