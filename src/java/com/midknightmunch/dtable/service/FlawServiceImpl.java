/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.Flaw;
import com.midknightmunch.dtable.repository.FlawRepository;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author KODE
 */
@Service
@Transactional
public class FlawServiceImpl implements FlawService{

    @Autowired
    FlawRepository flawRepository;
    
    @Override
    public Flaw create(Flaw t) {
        return flawRepository.save(t);
    }

    @Override
    public Flaw get(Integer id) {
        if(flawRepository.findById(id).isPresent()) {
    		return flawRepository.findById(id).get();
        }
    	else {
    		return null;
    	}
    }

    @Override
    public List<Flaw> getAll() {
        return flawRepository.findAll();
    }

    @Override
    public List<Flaw> getByBackground(Integer backgroundId) {
        return flawRepository.findByBackgroundId(backgroundId);
    }
    
}
