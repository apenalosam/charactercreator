/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.Align;
import java.util.List;
/**
 *
 * @author XMY6267
 */
public interface AlignService {
    public Align get(Integer alignId);
    public List<Align> findAll();
}
