/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.Ideal;
import com.midknightmunch.dtable.repository.IdealRepository;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author KODE
 */
@Service
@Transactional
public class IdealServiceImpl implements IdealService{

    @Autowired
    IdealRepository idealRepository;
    
    @Override
    public Ideal create(Ideal t) {
        return idealRepository.save(t);
    }

    @Override
    public Ideal get(Integer id) {
        if(idealRepository.findById(id).isPresent()) {
    		return idealRepository.findById(id).get();
        }
    	else {
    		return null;
    	}
    }

    @Override
    public List<Ideal> getAll() {
        return idealRepository.findAll();
    }

    @Override
    public List<Ideal> getByBackground(Integer backgroundId) {
        return idealRepository.findByBackgroundId(backgroundId);
    }
    
}
