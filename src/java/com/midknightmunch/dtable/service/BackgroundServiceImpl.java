/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.Background;
import com.midknightmunch.dtable.repository.BackgroundRepository;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author KODE
 */
@Service
@Transactional
public class BackgroundServiceImpl implements BackgroundService{

    @Autowired
    BackgroundRepository backgroundRepository;
    
    @Override
    public Background get(Integer id) {
    	if(backgroundRepository.findById(id).isPresent()) {
    		return backgroundRepository.findById(id).get();
        }
    	else {
    		return null;
    	}
    }

    @Override
    public List<Background> getAll() {
        return backgroundRepository.findAll();
    }

    @Override
    public boolean exists(Integer id) {
        return backgroundRepository.existsById(id);
    }

    @Override
    public Background create(Background b) {
        return backgroundRepository.save(b);
    }
    
}
