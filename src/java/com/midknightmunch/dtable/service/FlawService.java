/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.Flaw;
import java.util.List;

/**
 *
 * @author KODE
 */
public interface FlawService {
    public Flaw create(Flaw t);
    public Flaw get(Integer id);
    public List<Flaw> getAll();
    public List<Flaw> getByBackground(Integer backgroundId);
}
