/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.Background;
import java.util.List;

/**
 *
 * @author KODE
 */
public abstract interface BackgroundService {
    public abstract Background get(Integer id);
    public abstract List<Background> getAll();
    public abstract boolean exists(Integer id);
    public abstract Background create(Background b);
}
