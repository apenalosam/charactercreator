/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.BackgroundInfo;
import java.util.List;

/**
 *
 * @author KODE
 */
public interface BondService {
    public BackgroundInfo create(BackgroundInfo t);
    public BackgroundInfo get(Integer id);
    public List<BackgroundInfo> getAll();
    public List<BackgroundInfo> getByBackground(Integer backgroundId);
}
