/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.Ideal;
import java.util.List;

/**
 *
 * @author KODE
 */
public interface IdealService {
    public Ideal create(Ideal t);
    public Ideal get(Integer id);
    public List<Ideal> getAll();
    public List<Ideal> getByBackground(Integer backgroundId);
}
