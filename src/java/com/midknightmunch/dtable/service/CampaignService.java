/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.Campaign;
import java.util.List;
import com.midknightmunch.dtable.util.CharacterSimple;

/**
 *
 * @author XMY6267
 */
public interface CampaignService {
    public Campaign create(Campaign c);
    public Campaign get(Integer campaignId);
    public Campaign update(Campaign c);
    public boolean exists(Integer campaignId);
    public boolean exists(String campaignName);
    public List<Object> showCampaigns(String username);
    public List<Object> showCharacters(Integer campaignId);
    public List<CharacterSimple> getUserJsonList(String query);
}
