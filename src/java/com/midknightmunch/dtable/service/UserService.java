/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.User;
import com.midknightmunch.dtable.util.AccessMenu;
import java.util.List;

/**
 *
 * @author KODE
 */
public interface UserService {
    void create(User user);
    User update(User user);
    void save(User user, Long role);
    User findByUsername(String username);
    User findByUsernameAndPassword(String username, String password);
    boolean exists(String username);    
    List<AccessMenu> getAccessMenu(String username);
}
