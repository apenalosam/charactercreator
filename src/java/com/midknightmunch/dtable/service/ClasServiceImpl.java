/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.Clas;
import com.midknightmunch.dtable.repository.ClasRepository;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author XMY6267
 */
@Service
@Transactional
public class ClasServiceImpl implements ClasService{
    
    
    @Autowired
    private ClasRepository clasRepository;    
    
    @Override
    public Clas create(Clas c) {
        return clasRepository.save(c);
    }

    @Override
    public Clas get(Integer classId) {
    	if(clasRepository.findById(classId).isPresent()) {
    		return clasRepository.findById(classId).get();
        }
    	else {
    		return null;
    	}
    }

    @Override
    public List<Clas> getAll() {
        return clasRepository.findAll();
    }

    @Override
    public boolean exists(Integer classId) {
        return clasRepository.existsById(classId);
    }
    
}
