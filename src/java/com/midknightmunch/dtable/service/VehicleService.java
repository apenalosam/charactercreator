/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.Vehicle;
import java.util.List;

/**
 *
 * @author KODE
 */
public interface VehicleService {
    public Vehicle create(Vehicle v);
    public Vehicle get(Integer id);
    public List<Vehicle> getAll();
    public List<Vehicle> getByType(String type);
    public boolean exists(Integer id);
}
