/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.Campaign;
import com.midknightmunch.dtable.repository.CampaignRepository;
import com.midknightmunch.dtable.util.CampaignUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.midknightmunch.dtable.util.CharacterSimple;
import com.midknightmunch.dtable.util.CharacterUtil;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author XMY6267
 */
@Service
@Transactional
public class CampaignServiceImpl implements CampaignService{
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    CampaignRepository campaignRepository;

    @Override
    public Campaign create(Campaign c) {
        return campaignRepository.save(c);
    }

    @Override
    public Campaign get(Integer campaignId) {
        return campaignRepository.findByCampaignId(campaignId);
    }

    @Override
    public Campaign update(Campaign c) {
        Campaign camp = get(c.getCampaignId());
        camp.update(c);
        return camp;
    }

    @Override
    public boolean exists(Integer campaignId) {
        return campaignRepository.existsById(campaignId);
    }

    @Override
    public boolean exists(String campaignName) {
        StringBuilder sql = new StringBuilder("SELECT count(campaign_name) AS total");
        sql.append(" FROM campaigns ");
        sql.append(" WHERE campaign_name = ").append(campaignName);
        boolean existe = false;
        List<Object> l = entityManager.createNativeQuery(sql.toString()).getResultList();
        if(!l.isEmpty()) {
            if(Integer.parseInt(String.valueOf(l.get(0))) > 0) {
                existe = true;
            }
        }
        return existe ;
    }
    
    @Override
    public List<Object> showCampaigns(String username){
        StringBuilder sql = new StringBuilder("SELECT ");
        sql.append("campaign_name, active, campaign_id");
        sql.append(" FROM dynamic_dnd.campaigns");
        sql.append(" WHERE (master_id = '");
        sql.append(username).append("');");
        return CampaignUtil.getCampaign(entityManager.createNativeQuery(sql.toString()).getResultList());
    }
    
    @Override
    public List<Object> showCharacters(Integer campaignId){
        StringBuilder sql = new StringBuilder("SELECT ");
        sql.append("character_name, username, character_id");
        sql.append(" FROM dynamic_dnd.characters");
        sql.append(" WHERE (campaign_id = '");
        sql.append(campaignId).append("');");
        List <Object> tmp = CampaignUtil.getCharacter(entityManager.createNativeQuery(sql.toString()).getResultList());
        for(int i = 0; i<tmp.size(); i++){
            String tmp2 = tmp.get(i).toString();
            tmp2 = tmp2.replace("[", "");
            tmp2 = tmp2.replace("]", "");
            tmp.set(i, (Object)tmp2);
        }
        return tmp;
    }
        
    @Override
    public List<CharacterSimple> getUserJsonList(String query){
        StringBuilder sql = new StringBuilder("SELECT ");
        sql.append(" character_id, character_name, username");
        sql.append(" FROM dynamic_dnd.characters ");
        sql.append(CharacterUtil.searchRegistryForCampaign(query));
        return CampaignUtil.getCampaignUsers((List<Object[]>)entityManager.createNativeQuery(sql.toString()).getResultList());
    }
}
