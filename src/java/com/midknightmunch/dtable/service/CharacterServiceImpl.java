/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.Character;
import com.midknightmunch.dtable.repository.CharacterRepository;
import com.midknightmunch.dtable.util.CharacterUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author KODE
 */
@Service
@Transactional
public class CharacterServiceImpl implements CharacterService{

    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    private CharacterRepository characterRepository;
    
    @Override
    public Character create(Character c) {
        Character charac =  characterRepository.save(c);
        StringBuilder query = new StringBuilder("UPDATE ");
        query.append("dynamic_dnd.character SET creation_date = NOW() WHERE character_id = ");
        query.append(String.valueOf(c.getCharacterId())).append(";");
        return charac;
    }

    @Override
    public Character get(Integer characterId) {
    	if(characterRepository.findById(characterId).isPresent()) {
    		return characterRepository.findById(characterId).get();
        }
    	else {
    		return null;
    	}
    }

    @Override
    public List<Character> getAll() {
        return characterRepository.findAll();
    }

    @Override
    public List<Character> getByUsername(String username) {
        return characterRepository.findByUsername(username);
    }

    @Override
    public List<Character> getByCampaignId(int campaignId) {
         return characterRepository.findByCampaignId(campaignId);
    }

    @Override
    public Character update(Character c) {
        Character cha = get(c.getCharacterId());
        cha.update(c);
        return cha;
    }

    @Override
    public boolean exists(Integer id) {
        return characterRepository.existsById(id);
    }
    
    @Override
    public List<Object> showCharacters (String username){        
        StringBuilder sql = new StringBuilder("SELECT ");
        sql.append("c.character_name, cl.class_name, c.lvl, r.race_name, camp.campaign_name, c.character_id");
        sql.append(" FROM dynamic_dnd.characters AS c ");
        sql.append(" INNER JOIN dynamic_dnd.campaigns AS camp ON c.campaign_id = camp.campaign_id ");
        sql.append(" INNER JOIN dunganddrag.clas AS cl ON c.class_id = cl.class_id ");
        sql.append(" INNER JOIN dunganddrag.race AS r ON c.race_id = r.race_id ");
        sql.append("WHERE (c.username = '");
        sql.append(username).append("');");
        return CharacterUtil.getCharacter(entityManager.createNativeQuery(sql.toString()).getResultList());
    }
    @Override
    public int totalRegistry(String query) {
        StringBuilder sql = new StringBuilder("SELECT ");
        sql.append(" c.character_id ");
        sql.append(" FROM characters AS c INNER JOIN campaigns AS camp");
        sql.append(CharacterUtil.searchRegistry(query));
        return entityManager.createNativeQuery(sql.toString()).getResultList().size();
    }
}
