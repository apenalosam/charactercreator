/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.Align;
import com.midknightmunch.dtable.repository.AlignRepository;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author XMY6267
 */
@Service
@Transactional
public class AlignServiceImpl implements AlignService{

    @Autowired
    private AlignRepository alignRepository;
    @Override
    public Align get(Integer alignId) {
    	if(alignRepository.findById(alignId).isPresent()) {
    		return alignRepository.findById(alignId).get();
        }
    	else {
    		return null;
    	}
    }

    @Override
    public List<Align> findAll() {
        return alignRepository.findAll();
    }
    
}
