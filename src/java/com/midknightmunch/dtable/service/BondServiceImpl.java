/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.BackgroundInfo;
import com.midknightmunch.dtable.repository.BondRepository;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author KODE
 */
@Service
@Transactional
public class BondServiceImpl implements BondService{

    @Autowired
    BondRepository bondRepository;
    
    @Override
    public BackgroundInfo create(BackgroundInfo t) {
        return bondRepository.save(t);
    }

    @Override
    public BackgroundInfo get(Integer id) {
        if(bondRepository.findById(id).isPresent()) {
    		return bondRepository.findById(id).get();
        }
    	else {
    		return null;
    	}
    }

    @Override
    public List<BackgroundInfo> getAll() {
        return bondRepository.findAll();
    }

    @Override
    public List<BackgroundInfo> getByBackground(Integer backgroundId) {
        return bondRepository.findByBackgroundId(backgroundId);
    }
    
}
