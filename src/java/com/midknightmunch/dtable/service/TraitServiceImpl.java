/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.service;

import com.midknightmunch.dtable.model.Trait;
import com.midknightmunch.dtable.repository.TraitRepository;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author KODE
 */
@Service
@Transactional
public class TraitServiceImpl implements TraitService{

    @Autowired
    TraitRepository traitRepository;
    
    @Override
    public Trait create(Trait t) {
        return traitRepository.save(t);
    }

    @Override
    public Trait get(Integer id) {
        return traitRepository.findById(id).get();
    }

    @Override
    public List<Trait> getAll() {
        return traitRepository.findAll();
    }

    @Override
    public List<Trait> getByBackground(Integer backgroundId) {
        return traitRepository.findByBackgroundId(backgroundId);
    }
    
}
