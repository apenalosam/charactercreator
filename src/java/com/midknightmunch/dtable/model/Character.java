/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.model;

import java.io.Serializable;
import com.midknightmunch.dtable.util.CharacterUtil;
import com.midknightmunch.dtable.util.Util;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Andrés
 */
@Entity
@Table(name="characters", schema="dynamic_dnd")
public class Character implements Serializable{
    @Id
    @Column(name="character_id")
    private int characterId;
    @Column(name="username")
    private String username;
    @Column(name="character_name")
    private String characterName;
    @Column(name="player_name")
    private String playerName;
    @Column(name="class_id")
    private int classId;
    @Column(name="lvl")
    private int lvl;
    @Column(name="race_id")
    private int raceId;
    @Column(name="background_id")
    private int backgroundId;
    @Column(name="campaign_id")
    private int campaignId;
    @Column(name="added_str")
    private int str;
    @Column(name="added_dex")
    private int dex;
    @Column(name="added_con")
    private int con;
    @Column(name="added_int")
    private int intl;
    @Column(name="added_wis")
    private int wis;
    @Column(name="added_cha")
    private int cha;
    @Column(name="exp_points")
    private int expPoints;
    @Column(name="creation_date")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar creationDate;
    @Column(name="align_id")
    private int alignId;
    @Column(name="cp")
    private int cp;
    @Column(name="sp")
    private int sp;
    @Column(name="gp")
    private int gp;
    @Column(name="pp")
    private int pp;
    @Column(name="inventory")
    private String inventory;
    @Column(name="current_hp")
    private int currentHp;
    @Column(name="max_hp")
    private int maxHp;
    @Column(name="alive")
    private boolean alive;
    @Column(name="bonds", columnDefinition ="json")
    private String bonds;
    @Column(name="traits", columnDefinition ="json")
    private String traits;
    @Column(name="ideals", columnDefinition ="json")
    private String ideals;
    @Column(name="flaws", columnDefinition ="json")
    private String flaws;
    @Column(name="feats", columnDefinition ="json")
    private String feats;
    @Column(name="other_prof_lang", columnDefinition ="json")
    private String profiAndLang;
    @Column(name="attacks_spells", columnDefinition ="json")
    private String attacksAndSpells;
    @Column(name="weapons", columnDefinition ="json")
    private String weapons;
    @Column(name="available_points")
    private int availablePoints;
    @Column(name="last_level_set")
    private int lastLevelSet;
    @Column(name="armor_value")
    private int armorValue;
    @Column(name="shield_value")
    private int shieldValue;
    @Column(name="misc_value")
    private int miscValue;
    @Column(name="initiative")
    private int initiative;
    @Column(name="checkboxes")
    private String checkboxes;
    @Column(name="insp_bonus")
    private int inspBonus;
    @Transient
    private List<Weapon> weaponsList;
    @Transient
    private List<BackgroundInfo> bondsList;
    @Transient
    private List<Trait> traitsList;
    @Transient
    private List<Ideal> idealsList;
    @Transient
    private List<Flaw> flawsList;
    @Transient   
    private List<Feat> featsList;
    @Transient
    private List<String> saves;  
    @Transient
    private final String[] fixedSaves = {"athstr", "acrdex", "sledex", "stedex", "arcint", "hisint", "invint", "natint", "relint", "aniwis", "inswis", "medwis", "perwis", "surwis", "deccha", "inicha", "pefcha", "pescha"};
    
    
    
    public Character() {
    }
    
    public Character(HttpServletRequest request) {
        this.characterId = Integer.parseInt(request.getParameter("characterId"));
        this.characterName = Util.decode(request.getParameter("characterName"));
        this.playerName = Util.decode(request.getParameter("playerName"));
        this.classId = Integer.parseInt(request.getParameter("classId"));
        this.campaignId = Integer.parseInt(request.getParameter("campaignId"));
        this.raceId = Integer.parseInt(request.getParameter("raceId"));
        this.backgroundId = Integer.parseInt(request.getParameter("backgroundId"));
        this.lvl = Integer.parseInt(request.getParameter("lvl"));
        this.str = Integer.parseInt(request.getParameter("str"));
        this.dex = Integer.parseInt(request.getParameter("dex"));
        this.con = Integer.parseInt(request.getParameter("con"));
        this.intl = Integer.parseInt(request.getParameter("intl"));
        this.wis = Integer.parseInt(request.getParameter("wis"));
        this.cha = Integer.parseInt(request.getParameter("cha"));
        this.alignId = Integer.parseInt(request.getParameter("alignId"));
        this.expPoints = Integer.parseInt(request.getParameter("expPoints"));
        this.raceId = Integer.parseInt(request.getParameter("raceId"));
        this.cp = Integer.parseInt(request.getParameter("cp"));
        this.sp = Integer.parseInt(request.getParameter("sp"));
        this.gp = Integer.parseInt(request.getParameter("gp"));
        this.pp = Integer.parseInt(request.getParameter("pp"));
        this.currentHp = Integer.parseInt(request.getParameter("currentHp"));
        this.maxHp = Integer.parseInt(request.getParameter("maxHp"));
        this.alive = Boolean.valueOf(request.getParameter("alive"));
        this.bonds = Util.decode(request.getParameter("bonds"));
        this.traits = Util.decode(request.getParameter("traits"));
        this.ideals = Util.decode(request.getParameter("ideals"));
        this.flaws = Util.decode(request.getParameter("flaws"));
        this.feats = Util.decode(request.getParameter("features")).replace("'", "\'");
        this.profiAndLang = Util.decode(request.getParameter("profnlang")).replace("'", "\'");
        this.attacksAndSpells = Util.decode(request.getParameter("attnspells"));        
        this.weapons = CharacterUtil.getWeapon(request, "weapon1", "atkBns1", "dmgType1");
        this.availablePoints = Integer.parseInt(request.getParameter("availablePoints"));
        this.lastLevelSet = Integer.parseInt(request.getParameter("lastLevelSet"));
        this.armorValue = Integer.parseInt(request.getParameter("armorValueIn"));
        this.shieldValue = Integer.parseInt(request.getParameter("shieldValueIn"));
        this.miscValue = Integer.parseInt(request.getParameter("miscValueIn"));
        this.initiative = Integer.parseInt(request.getParameter("initiativeIn"));
        this.inventory = Util.decode(request.getParameter("inventory")).replace("'", "\'");
        this.inspBonus = Integer.parseInt(request.getParameter("inspBonusIn"));
        int i = 0;
        for(String save: fixedSaves){
            try{        
                if(i<fixedSaves.length-2)
                    this.checkboxes += Util.decode(request.getParameter(save))+"&;";
                else
                    this.checkboxes += Util.decode(request.getParameter(save));
                i++;
            }
            catch(NullPointerException e){
                continue;
            }
        }
        try{
            this.checkboxes = this.checkboxes.substring(4, this.checkboxes.length()-2);
        }catch(NullPointerException e){
            this.checkboxes = "";
        }
    }

    public Character update(Character chara){
        this.alignId = chara.getAlignId();
        this.campaignId = chara.getCampaignId();
        this.cha = chara.getCha();
        this.characterName = chara.getCharacterName();
        this.classId = chara.getClassId();
        this.con = chara.getCon();
        this.dex = chara.getDex();
        this.expPoints = chara.getExpPoints();
        this.intl = chara.getIntl();
        this.lvl = chara.getLvl();
        this.playerName = chara.getPlayerName();
        this.raceId = chara.getRaceId();
        this.str = chara.getStr();
        this.username = chara.getUsername();
        this.wis = chara.getWis();
        this.cp = chara.getCp();
        this.sp = chara.getSp();
        this.gp = chara.getGp();
        this.pp = chara.getPp();
        this.inventory = chara.getInventory();
        this.currentHp = chara.getCurrentHp();
        this.maxHp = chara.getMaxHp();
        this.alive = chara.isAlive();
        this.bonds = chara.getBonds();
        this.traits = chara.getTraits();
        this.ideals = chara.getIdeals();
        this.flaws = chara.getFlaws();
        this.feats = chara.getFeats();
        this.profiAndLang = chara.getProfiAndLang();
        this.attacksAndSpells = chara.getAttacksAndSpells();
        this.weapons = chara.getWeapons();
        this.availablePoints = chara.getAvailablePoints();
        this.lastLevelSet = chara.getLastLevelSet();
        this.armorValue = chara.getArmorValue();
        this.shieldValue = chara.getShieldValue();
        this.miscValue = chara.getMiscValue();
        this.initiative = chara.getInitiative();
        this.saves = chara.getSaves();
        this.checkboxes = chara.getCheckboxes();
        this.inspBonus = chara.getInspBonus();
        return this;
    }

    public int getInspBonus() {
        return inspBonus;
    }

    public void setInspBonus(int inspBonus) {
        this.inspBonus = inspBonus;
    }

    public String getCheckboxes() {
        return checkboxes;
    }

    public void setCheckboxes(String checkboxes) {
        this.checkboxes = checkboxes;
    }

    public List<String> getSaves() {
        return saves;
    }

    public void setSaves(List<String> saves) {
        this.saves = saves;
    }

    public int getInitiative() {
        return initiative;
    }

    public void setInitiative(int initiative) {
        this.initiative = initiative;
    }

    public int getArmorValue() {
        return armorValue;
    }

    public void setArmorValue(int armorValue) {
        this.armorValue = armorValue;
    }

    public int getShieldValue() {
        return shieldValue;
    }

    public void setShieldValue(int shieldValue) {
        this.shieldValue = shieldValue;
    }

    public int getMiscValue() {
        return miscValue;
    }

    public void setMiscValue(int miscValue) {
        this.miscValue = miscValue;
    }

    public int getLastLevelSet() {
        return lastLevelSet;
    }

    public void setLastLevelSet(int lastLevelSet) {
        this.lastLevelSet = lastLevelSet;
    }

    public int getAvailablePoints() {
        return availablePoints;
    }

    public void setAvailablePoints(int availablePoints) {
        this.availablePoints = availablePoints;
    }

    
    public String getBonds() {
        return bonds;
    }

    public void setBonds(String bonds) {
        this.bonds = bonds;
    }

    public String getTraits() {
        return traits;
    }

    public void setTraits(String traits) {
        this.traits = traits;
    }

    public String getIdeals() {
        return ideals;
    }

    public void setIdeals(String ideals) {
        this.ideals = ideals;
    }

    public String getFlaws() {
        return flaws;
    }

    public void setFlaws(String flaws) {
        this.flaws = flaws;
    }

    public String getFeats() {
        return feats;
    }

    public void setFeats(String feats) {
        this.feats = feats;
    }

    public String getProfiAndLang() {
        return profiAndLang;
    }

    public void setProfiAndLang(String profiAndLang) {
        this.profiAndLang = profiAndLang;
    }

    public String getAttacksAndSpells() {
        return attacksAndSpells;
    }

    public void setAttacksAndSpells(String attacksAndSpells) {
        this.attacksAndSpells = attacksAndSpells;
    }

    public String getWeapons() {
        return weapons;
    }

    public void setWeapons(String weapons) {
        this.weapons = weapons;
    }
    
    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }
    
    public int getCurrentHp() {
        return currentHp;
    }

    public void setCurrentHp(int currentHp) {
        this.currentHp = currentHp;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    public int getSp() {
        return sp;
    }

    public void setSp(int sp) {
        this.sp = sp;
    }

    public int getGp() {
        return gp;
    }

    public void setGp(int gp) {
        this.gp = gp;
    }

    public int getPp() {
        return pp;
    }

    public void setPp(int pp) {
        this.pp = pp;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public int getStr() {
        return str;
    }

    public void setStr(int str) {
        this.str = str;
    }

    public int getDex() {
        return dex;
    }

    public void setDex(int dex) {
        this.dex = dex;
    }

    public int getCon() {
        return con;
    }

    public void setCon(int con) {
        this.con = con;
    }

    public int getIntl() {
        return intl;
    }

    public void setIntl(int intl) {
        this.intl = intl;
    }

    public int getWis() {
        return wis;
    }

    public void setWis(int wis) {
        this.wis = wis;
    }

    public int getCha() {
        return cha;
    }

    public void setCha(int cha) {
        this.cha = cha;
    }

    public int getCharacterId() {
        return characterId;
    }

    public void setCharacterId(int characterId) {
        this.characterId = characterId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public int getRaceId() {
        return raceId;
    }

    public void setRaceId(int raceId) {
        this.raceId = raceId;
    }

    public int getBackgroundId() {
        return backgroundId;
    }

    public void setBackgroundId(int backgroundId) {
        this.backgroundId = backgroundId;
    }

    public int getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    public int getExpPoints() {
        return expPoints;
    }

    public void setExpPoints(int expPoints) {
        this.expPoints = expPoints;
    }

    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public int getAlignId() {
        return alignId;
    }

    public void setAlignId(int alignId) {
        this.alignId = alignId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    @Override
    public String toString() {
        return "Character{" + "characterId=" + characterId + ", username=" + username + ", characterName=" + characterName + ", playerName=" + playerName + ", classId=" + classId + ", lvl=" + lvl + ", raceId=" + raceId + ", backgroundId=" + backgroundId + ", campaignId=" + campaignId + ", str=" + str + ", dex=" + dex + ", con=" + con + ", intl=" + intl + ", wis=" + wis + ", cha=" + cha + ", expPoints=" + expPoints + ", creationDate=" + creationDate + ", alignId=" + alignId + ", cp=" + cp + ", sp=" + sp + ", gp=" + gp + ", pp=" + pp + ", inventory=" + inventory + ", currentHp=" + currentHp + ", maxHp=" + maxHp + ", alive=" + alive + '}';
    }

    public int getMaxHp() {
        return maxHp;
    }

    public void setMaxHp(int maxHp) {
        this.maxHp = maxHp;
    }
    
}
