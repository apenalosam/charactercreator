/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.servlet.http.HttpServletRequest;
import com.midknightmunch.dtable.util.Util;

/**
 *
 * @author XMY6267
 */
@Entity
@Table(name="campaigns", schema="dynamic_dnd")
public class Campaign implements Serializable{
    @Id
    @Column(name="campaign_id")
    private int campaignId;
    @Column(name="active")
    private boolean active;
    @Column(name="campaign_name")
    private String campaignName;
    @Column(name="master_id")
    private String masterId;

    public Campaign update(Campaign c){
        this.campaignId = c.campaignId;
        this.campaignName = c.campaignName;
        this.active = c.active;
        return this;
    }
    
    public Campaign() {
    }

    public Campaign(int campaignId, boolean active, String campaignName) {
        this.campaignId = campaignId;
        this.active = active;
        this.campaignName = campaignName;
    }
    
    public Campaign(HttpServletRequest request, int id){ 
        this.campaignId = id;
        this.campaignName = Util.decode(request.getParameter("campaignName"));
        this.active = true;             
    }

    public int getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }
}
