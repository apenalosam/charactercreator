/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author KODE
 */
@Entity
@Table(name="background_infos")
public class BackgroundInfo implements Serializable{
    @Id
    @Column(name="id")
    private int id;
    @Column(name="background_id")
    private int backgroundId;
    @Column(name="description")
    private String description;
    @Column(name="name")
    private String name;

    public BackgroundInfo(int id, int backgroundId, String description, String name) {
        this.id = id;
        this.backgroundId = backgroundId;
        this.description = description;
        this.name = name;
    }

    public BackgroundInfo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBackgroundId() {
        return backgroundId;
    }

    public void setBackgroundId(int backgroundId) {
        this.backgroundId = backgroundId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
}
