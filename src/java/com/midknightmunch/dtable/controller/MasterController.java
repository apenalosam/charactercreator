/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.midknightmunch.dtable.model.Align;
import com.midknightmunch.dtable.model.Background;
import com.midknightmunch.dtable.model.Clas;
import com.midknightmunch.dtable.model.Race;
import com.midknightmunch.dtable.model.User;
import com.midknightmunch.dtable.model.Character;
import com.midknightmunch.dtable.service.AlignService;
import com.midknightmunch.dtable.service.BackgroundService;
import com.midknightmunch.dtable.service.CampaignService;
import com.midknightmunch.dtable.service.CharacterService;
import com.midknightmunch.dtable.service.ClasService;
import com.midknightmunch.dtable.service.RaceService;
import com.midknightmunch.dtable.service.UserService;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import com.midknightmunch.dtable.model.Campaign;
import com.midknightmunch.dtable.util.Util;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author KODE
 */
@Controller
@RequestMapping(value="/master")
public class MasterController {
    
     @PersistenceContext
    EntityManager entityManager;
    
    private static Logger LOGGER = Logger.getLogger(MainController.class);
    private final boolean isMaster = true;
    
    
    @Autowired
    UserService userService;
    @Autowired
    CharacterService characterService;
    @Autowired
    ClasService clasService;
    @Autowired
    AlignService alignService;
    @Autowired
    RaceService raceService;
    @Autowired
    BackgroundService backgroundService;
    @Autowired
    CampaignService campaignService;
    
    @RequestMapping(value="/mindex")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ModelAndView sheetManager(Principal thisUser){
        ModelAndView mav = new ModelAndView("master/mindex");
        mav.addObject("menu", this.userService.getAccessMenu(thisUser.getName()));
        mav.addObject("isMaster", true);
        return mav;
    }
    
    @RequestMapping(value="/campaign_selector", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_USER')")
    public @ResponseBody
    String selectChampaign (Principal thisUser){
        User user = userService.findByUsername(thisUser.getName());
        List<Object> listOfCampaigns;
        StringBuilder result = new StringBuilder();
        try{
            listOfCampaigns = campaignService.showCampaigns(user.getUsername());
            String tmp;
            for(Object o: listOfCampaigns){
                tmp = o.toString();
                result.append("<tr>");
                String[] items = tmp.split(", ");
                for(String s: items){
                    if(s.startsWith("<td>")){
                        result.append(s);
                    }
                    else{
                        if(s.startsWith("["))
                            s = s.substring(1);
                        result.append("<td class=\"text-center\">").append(s).append("</td>");
                    }
                }
                result.append("</tr>");
            }
        }
        catch(Exception e){
            LOGGER.error(e);
        }
        return result.toString();
    }
    
    @RequestMapping(value="sheet_manager/{campaignId}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ModelAndView sheetManager(Principal thisUser, @PathVariable int campaignId){
        ModelAndView mav = new ModelAndView("master/sheet_manager");
        List<Object> listOfCharacters;
        StringBuilder result = new StringBuilder();
        mav.addObject("isMaster", true);
        mav.addObject("menu", this.userService.getAccessMenu(thisUser.getName()));
        mav.addObject("campaignId", campaignId);
        try{
            listOfCharacters = campaignService.showCharacters(campaignId);
            String tmp;
            for(Object o: listOfCharacters){
                tmp = o.toString();
                result.append("<tr>");
                String[] items = tmp.split(", ");
                for(String s: items){
                    if(s.startsWith("<td>")){
                        result.append(s);
                    }
                    else{
                        if(s.startsWith("["))
                            s = s.substring(1);
                        result.append("<td class=\"text-center\">").append(s).append("</td>");
                    }
                }
                result.append("</tr>");
            }
        }
        catch(Exception e){
            LOGGER.error(e);
        }
        mav.addObject("table", result.toString());
        return mav;
    }
   
    @RequestMapping(value="/player_sheet/{characterId}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ModelAndView SheetDisplay(Principal thisUser, @PathVariable int characterId){
        ModelAndView mav = new ModelAndView("dtable/player_sheet"); 
        mav.addObject("isMaster", true);
        mav.addObject("isNew", false);
        LOGGER.info("MAV created");
        com.midknightmunch.dtable.model.Character character = characterService.get(characterId);
        LOGGER.info("Character created: "+character.getCharacterName());
        mav.addObject("c", character);
        LOGGER.info(character.getCharacterName()+" added to MAV");
        User user = userService.findByUsername(thisUser.getName());  
        LOGGER.info("User created");
        mav.addObject("user", user);
        LOGGER.info(user.getNames()+" added to MAV");
        Clas clas = clasService.get(character.getClassId());
        LOGGER.info("Class fetched");
        mav.addObject("cl", clas);
        LOGGER.info("Class added to MAV");
        List<Clas> selectClas = clasService.getAll();
        LOGGER.info("List of classes fetched");
        mav.addObject("classes", selectClas);
        LOGGER.info("List of classes added to MAV");
        Align align = alignService.get(character.getAlignId());
        List<Align> alignments = alignService.findAll();
        Race race = raceService.get(character.getRaceId());
        List<Race> races = raceService.findAll();
        List<Background> backgrounds = backgroundService.getAll();
        int profBonus;
        StringBuilder sql = new StringBuilder("SELECT prof_bonus FROM dunganddrag.class_abilities ");
        sql.append("WHERE (class_id = ").append(clas.getClassId());
        sql.append(" AND lvl = ").append(character.getLvl()).append(");");
        profBonus = (int) entityManager.createNativeQuery(sql.toString()).getResultList().get(0);
        LOGGER.info(profBonus);
        mav.addObject("r", race);
        mav.addObject("races", races);
        mav.addObject("al", align);
        mav.addObject("alignments", alignments);
        mav.addObject("backgrounds", backgrounds);
        mav.addObject("menu", this.userService.getAccessMenu(user.getUsername()));
        mav.addObject("btnName", "Update");
        mav.addObject("isNew", false);
        mav.addObject("profBonus", profBonus);
        return mav;
    }    
    
    @RequestMapping(value="/listOfCharacters")
    @PreAuthorize("hasRole('ROLE_USER')")
    public @ResponseBody
    String getBaseOperativoListadoAll(@RequestParam(value = "q") String query) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode objectNode = mapper.createObjectNode();
        objectNode.putPOJO("status", true);
        objectNode.putPOJO("error", null);
        objectNode.putPOJO("data", campaignService.getUserJsonList(query));
        String json = mapper.writeValueAsString(objectNode);
        return json;
    }
    
    @RequestMapping(value="/new_campaign", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_USER')")
    public @ResponseBody
    ModelAndView newCampaign(HttpServletRequest request, Principal thisUser){
        String query = "SELECT MAX(campaign_id) FROM dynamic_dnd.campaigns;";
        int maxId = Integer.parseInt(entityManager.createNativeQuery(query).getResultList().get(0).toString());
        maxId +=1;
        Campaign campaign = new Campaign(request, maxId);
        List<String> characterList = Util.stringToList(Util.decode(request.getParameter("characterList")), ",");
        List<Integer> idList = new ArrayList<>();
        characterList.forEach((entry) -> {
            idList.add(Integer.parseInt(entry));
        });
        for(int id:idList){
            Character c = characterService.get(id);
            c.setCampaignId(maxId);
            c = characterService.update(c);
        }
        //makeCampaign(Util.stringToList(Util.decode(request.getParameter("characterList")), ","), maxId);
        campaign.setMasterId(thisUser.getName());
        campaign = campaignService.create(campaign);
        return sheetManager(thisUser, maxId);
    }
    
    @RequestMapping(value="/add_characters/{campaignId}", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_USER')")
    public @ResponseBody
    ModelAndView addCharacters(HttpServletRequest request, Principal thisUser, @PathVariable int campaignId){
        List<String> characterList = Util.stringToList(Util.decode(request.getParameter("characterList")), ",");
        List<Integer> idList = new ArrayList<>();
        characterList.forEach((entry) -> {
            idList.add(Integer.parseInt(entry));
        });
        for(int id:idList){
            Character c = characterService.get(id);
            c.setCampaignId(campaignId);
            c = characterService.update(c);
        }
        return sheetManager(thisUser, campaignId);
    }
    
    public void makeCampaign(List<String> characterList, int campaignId) {
        
    }
}
