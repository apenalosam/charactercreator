/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.util;

import java.io.Serializable;

/**
 *
 * @author KODE
 */
public class CharacterSimple implements Serializable {
    private int characterId;
    private String characterName;
    private String username;

    public int getCharacterId() {
        return characterId;
    }

    public void setCharacterId(int characterId) {
        this.characterId = characterId;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public String getUserName() {
        return username;
    }

    public void setUserName(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "CharacterSimple{" + "characterId=" + characterId + ", characterName=" + characterName + ", username=" + username + '}';
    }
    
    
}
