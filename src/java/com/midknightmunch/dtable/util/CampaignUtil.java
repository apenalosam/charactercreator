/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.util;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author KODE
 */
public class CampaignUtil {
    
    
    public static List<Object> getCampaign(List<Object[]> datos) {
        List<Object> lista = new ArrayList<>();
        List<Object> data;        
        StringBuilder c;        
        for (Object[] d : datos) {
            data = new ArrayList<>();
            data.add(d[0]);
            if((Boolean)d[1]){
                data.add("Yes");
            }
            else{
                data.add("No");
            }
            c = new StringBuilder("<td><div class=\"text-center\">");
            c.append("<a href=\"javascript:void(0);\" ");
            c.append("onclick=\"loadCampaign(").append(d[2]).append(");\" ");
            //c.append("class=\"btn btn-primary\" >");
            c.append("class=\"btn btn-success \" >");
            c.append("<i class=\"fa fa-check ");
            c.append("load").append(d[2]).append("\" aria-hidden=\"true\"></i> </a>");
            c.append("</td></div>");
            data.add(c);
            lista.add(data);
        }
        return lista;
    }
    
    public static List<CharacterSimple> getCampaignUsers(List<Object[]> datos) {
        List<CharacterSimple> list = new ArrayList<>();
        CharacterSimple cs = null;
        for (Object[] d : datos) {
            cs = new CharacterSimple();
            cs.setCharacterId(Util.getIntegerValue(d[0]));
            cs.setCharacterName(Util.getString(d[1]));
            cs.setUserName(Util.getString(d[2]));
            list.add(cs);
        }
        return list;
    }
    
    public static List<Object> getCharacter(List<Object[]> datos) {
        List<Object> lista = new ArrayList<>();
        List<Object> data;        
        StringBuilder c;        
        for (Object[] d : datos) {
            data = new ArrayList<>();
            data.add(d[0]);
            data.add(d[1]);
            c = new StringBuilder("<td><div class=\"text-center\">");
            c.append("<a href=\"javascript:void(0);\" ");
            c.append("onclick=\"loadSheet(").append(d[2]).append(");\" ");
            //c.append("class=\"btn btn-primary\" >");
            c.append("class=\"btn btn-success \" >");
            c.append("<i class=\"fa fa-edit ");
            c.append("load").append(d[2]).append("\" aria-hidden=\"true\"></i> </a>");
            c.append("</td></div>");
            data.add(c);
            lista.add(data);
        }
        return lista;
    }

    
}
