package com.midknightmunch.dtable.repository;

import com.midknightmunch.dtable.model.User;

import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface UserRepository extends JpaRepository<User, String>{
    
    public abstract User findByUsername(String username);
    
    public abstract User findByUsernameAndPassword(String username, String password);
}
