package com.midknightmunch.dtable.repository;

import com.midknightmunch.dtable.model.Character;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
/**
 *
 * @author KODE
 */
public abstract interface CharacterRepository extends JpaRepository<Character, Integer>{
    public abstract List<Character> findByUsername(String username);
    public abstract List<Character> findByCampaignId(Integer campaignId);
}
