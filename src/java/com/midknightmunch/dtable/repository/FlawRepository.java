/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.repository;

import com.midknightmunch.dtable.model.Flaw;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author KODE
 */
public interface FlawRepository extends JpaRepository<Flaw, Integer>{
    public abstract List<Flaw> findByBackgroundId(Integer backgroundId);
}
