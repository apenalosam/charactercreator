/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midknightmunch.dtable.repository;

import com.midknightmunch.dtable.model.Tool;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author XMY6267
 */
public interface ToolRepository extends JpaRepository<Tool, Integer>{
    public abstract List<Tool> findByToolType(Integer typeId);
}
