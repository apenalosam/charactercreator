<%-- 
    Document   : error
    Created on : 30/07/2018, 12:56:50 PM
    Author     : XMY6267
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error</title>
    </head>
    <body>
        <div>
            <h1 style="width: 100%; text-align: center; font-size: 50px;">Error!</h1>
            <p style="margin-left: 10%; margin-top: 10%; font-size: 30px;">${msg}</p>
        </div>
    </body>
</html>
