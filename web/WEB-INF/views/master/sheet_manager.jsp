<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ include file="../header.jsp"%>
<%@ include file="../menu.jsp"%>
<link href="${contextPath}/resources/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="${contextPath}/resources/css/dtable.search.css" rel="stylesheet">
<link href="${contextPath}/resources/css/dataTables.bootstrap.min.css" rel="stylesheet">
<script src="${contextPath}/resources/js/functions.js"></script>
<link href="${contextPath}/resources/css/dtable.search.css" rel="stylesheet">
<link href="${contextPath}/resources/js/plugins/toast/jquery.toast.css" rel="stylesheet">
<link href="${contextPath}/resources/js/plugins/jquery.typeahead.min.css" rel="stylesheet">
<script src="${contextPath}/resources/js/plugins/jquery.typeahead.min.js"></script>
<div class="modal-body" id="modalBody" style="padding-top: 100px;">
    <table id="listOfCharacters" class="table table-bordered charTable" cellspacing="0" width="100%">
        <thead class="stylish" style="background-color: #800000; border-color: #800000;">
            <tr class="btn-primary">
                <th style="width: 18%;" class="table-header">Character Name</th>
                <th style="width: 18%;" class="table-header">Player Username</th>
                <th style="width: 10%;" class="table-header">Edit</th>
            </tr>
        </thead>
        <tbody id="characters">
        </tbody>
    </table>
    <div style="margin-top: 4%; padding-left: 45%; width:100%;">
        <button id="addButton" style="background-color: transparent; border-color: transparent" class="stylish btn-primary"><i class="fa-stack fa-retro ">
                <i class="fa fa-circle fa-stack-2x" style="color:grey;"></i>
                <i class="fa fa-plus-circle fa-stack-1x" style="color:#800000;"></i>
            </i>&nbsp;Add Character</button>
    </div>
</div>
<div id="addCharactersModal" class="modal fade" role="dialog" style="margin-top: 10%;">
    <div class="modal-dialog">
        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="panel-title" class="stylish"><strong>Add characters</strong></h3>
            </div>
            <div class="modal-body" id="newModalBody">
                <form:form action="${contextPath}/master/add_characters/${campaignId}" name="addCharactersForm" id="addCharactersForm" method="POST" enctype="multipart/form-data" accept-charset="UTF-8" autocomplete="off">
                    <br/>
                    <div>
                        <label class="stylish">Add users (search by username):</label>
                        <div class="typeahead__container">
                            <div class="typeahead__field">
                                <span class="typeahead__query">
                                    <input class="js-base stylish" id="search_campaign" type="search" placeholder="Search users..." autocomplete="off">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div style="margin-top: 20px;">
                        <table id="selectedCharacters" class="table table-striped table-bordered hide" cellspacing="0" width="100%">
                            <thead class="stylish" style="background-color: #800000; border-color: #800000;">
                                <tr class="btn-primary">
                                    <th style="width: 70%;" class="table-header">Character Name</th>
                                    <th style="width: 20%;" class="table-header">Username</th>
                                </tr>
                            </thead>
                            <tbody id="insertCharacters">
                            </tbody>
                        </table>
                        <input type="hidden" name="characterList" id="characterList">
                    </div>
                </form:form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-default" onclick="addNewCharacters()">Add these characters</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    var characterIds = [];
    $(document).ready(function () {
        document.getElementById('characters').innerHTML += '${table}';
        $('#addButton').click(function(){
            $('#addCharactersModal').modal('show');
        });
        $.typeahead({
            input: '.js-base',
            minLength: 1,
            maxItem: 15, //hasta 15 registro muestra
            hint: true,
            cache: false,
            template: '<div class="ProfileCard u-cf">'+
                      '<table width="100%">'+
                      '<tr>'+
                      '<td width="15%"><strong><i>Username:</i></strong>&nbsp</td>'+
                      '<td width="35%">{{userName}}</td>'+
                      '<td width="15%"><strong><i>Character Name:</i><strong>&nbsp</td>'+
                      '<td width="35%">{{characterName}}</td>'+
                      '</tr>'+
                      '</table>'+
                      '</div>',
            emptyTemplate: '<div class="MensajeVacio">The user/character {{query}} doesn\'t exist<br>Please check your spelling and make sure that the character has been created</div>',
            dynamic: true,
            source: {
                base: {
                    display: ["userName", "characterName"],
                    ajax: {
                        url: "${contextPath}/master/listOfCharacters",
                        path: "data",
                        data: {
                            q: "{{query}}"
                        }
                    }
                }
            },
            callback: {
                onNavigateBefore: function (node, query, event) {
                    if (~[38, 40].indexOf(event.keyCode)) {
                        event.preventInputChange = true;
                    }
                },
                onClick: function (node, a, item, event) {
                    var html = $('#insertCharacters').html();
                    if(characterIds.includes(item.characterId)===false){
                        characterIds.push(item.characterId);
                        html += '<tr><td>'+item.characterName+'</td>';
                        html += '<td>'+item.userName+'</td></tr>';
                    }
                    $('#insertCharacters').html(html);
                    var chLen = characterIds.length;
                    var charIds = characterIds[0];
                    for (var i=1; i<chLen; i++){
                        charIds += ","+characterIds[i];
                    }
                    $('#characterList').val(charIds);
                    $('#selectedCharacters').removeClass('hide');
                }
            }
        });
    });
    function loadSheet(characterId){
        $('.load'+characterId).removeClass('fa-edit');
        $('.load'+characterId).addClass('fa-spinner fa-spin');
        setPath('player_sheet/' + characterId);
    }
    function setPath(pathId){
        window.location.href='${contextPath}/master/'+pathId;
    }
</script>
<%@ include file="../footer.jsp" %>