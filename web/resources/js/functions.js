/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var addedRaceStats = false;
var addedClasStats = false;
var raceSelected = false;
var classSelected = false;
var raisedStats;
$(document).ready(function () {
    resizable(document.getElementById('characterId'), 10);
    $('#isSaved').val(false);
    $('#lvlUp').prop('disabled', true);
    $('#expPoints').change(function () {
        checkExp();
    });
    $('.defValue').focusout(function(){
        if($(this).html()===''){
            $(this).html('0');
        }
        var inputVal = this.id+"In";
        $('#'+inputVal).val(parseInt($(this).html()));
        setArmorClass();
    });
    if (!isNew) {
        setCharacter();  
        addedRaceStats = true;
        checkAlive();
        if (!isMaster) {
            $('.inner-stat').prop('disabled', true);
            $('#classId').prop('disabled', true);
            $('#raceId').prop('disabled', true);
            $('#backgroundId').prop('disabled', true);
            document.getElementById('inspBonus').contentEditable = 'false';
        } else {
            $('#playerName').prop('disabled', true);
        }
    }
    if(availablePoints > 0)
        improveAbilityScore(availablePoints);
    $('#lvl').prop("disabled", true);
    $('#strModifier').html(setMod($('#str').val()));
    $('#dexModifier').html(setMod($('#dex').val()));
    $('#conModifier').html(setMod($('#con').val()));
    $('#intModifier').html(setMod($('#intl').val()));
    $('#wisModifier').html(setMod($('#wis').val()));
    $('#chaModifier').html(setMod($('#cha').val()));
    $('#raceId').change(function () {  
        if($(this).val() !== -1)
            raceSelected = true;
        else
            raceSelected = false;
        getRace($(this).val());
        if (raceSelected && classSelected) 
            firstAbilityScores();
    });
    $('#classId').change(function () {
        if($(this).val() !== -1)
            classSelected = true;
        else
            classSelected = false;
        getClas($(this).val());
        $('#lvl').val(1);
        if (raceSelected && classSelected) 
            firstAbilityScores();
    });
    $('#alignId').change(function () {
        getAlign($(this).val());
    });
    $('#success2').prop("disabled", true);
    $('#success3').prop("disabled", true);
    $('#failure2').prop("disabled", true);
    $('#failure3').prop("disabled", true);
    $('input:radio[name=successes]').change(function () {
        if ($(this).attr("id") === "success3") {
            alert("You survived");
            $('#success1').prop("disabled", false);
            document.getElementById("success3").checked = false;
            document.getElementById("failure1").checked = false;
            document.getElementById("failure2").checked = false;
            document.getElementById("failure3").checked = false;
            $('#failure1').prop("disabled", false);
            $('#failure2').prop("disabled", true);
            $('#failure3').prop("disabled", true);
        } else {
            var id = $(this).attr("id");
            id = id.substring(id.length - 1);
            id = parseInt(id) + 1;
            $('#success' + id).prop("disabled", false);
        }
        $(this).prop("disabled", true);
    });
    $('input:radio[name=failures]').change(function () {
        if ($(this).attr("id") === "failure3") {
            changeAlive();
        } else {
            var id = $(this).attr("id");
            id = id.substring(id.length - 1);
            id = parseInt(id) + 1;
            $('#failure' + id).prop("disabled", false);
        }
        $(this).prop("disabled", true);
    });
    if (isMaster) {
        $('#dthMsgLbl').html('This character is dead');
        $('#revive').removeClass('hide');
        $('#classId').prop('disabled', false);
        $('#raceId').prop('disabled', false);
        $('#backgroundId').prop('disabled', false);
        $('#lvl').prop("disabled", false);
        document.getElementById('revive').className = document.getElementById('revive').className.replace("hide", "show");
    } else {
        $('#dthMsgLbl').html('YOU ARE DEAD');
        $('#revive').addClass('hide');
    }
    checkExp();
});

function reviveCharacter() {
    checkAlive();
    if ($('#alive').val() === false) {
        changeAlive();
    }
}

function setMod(stat) {
    if (stat > 19) {
        return '+5';
    } else if (stat > 17) {
        return '+4';
    } else if (stat > 15) {
        return '+3';
    } else if (stat > 13) {
        return '+2';
    } else if (stat > 11) {
        return '+1';
    } else if (stat > 9) {
        return '+0';
    } else if (stat > 7) {
        return '-1';
    } else if (stat > 5) {
        return '-2';
    } else if (stat > 3) {
        return '-3';
    } else {
        return '-4';
    }
}
function resizable(el, factor) {
    var int = Number(factor) || 7.7;
    function resize() {
        el.style.width = ((el.value.length + 1) * int) + 'px';
    }
    var e = 'keyup,keypress,focus,blur,change'.split(',');
    for (var i in e)
        el.addEventListener(e[i], resize, false);
    resize();
}
function setHp(hitDie, hpAdd) {
    var maxHp = parseInt(hitDie) + parseInt($('#conModifier').text()) + parseInt((hpAdd * ($('#lvl').val() - 1)));
    $('#maxHpLbl').html(maxHp);
    if (isNew)
        $('#currentHp').val(maxHp);
}

function enable() {
    $('#lvl').prop('disabled', false);
    $('#maxHp').val($('#maxHpLbl').html());
    $('#classId').prop('disabled', false);
    $('#raceId').prop('disabled', false);
    $('#backgroundId').prop('disabled', false);
    $('#playerName').prop('disabled', false);
    $('.inner-stat').prop('disabled', false);
}

function checkExp() {
    var exp = parseInt($('#expPoints').val());
    var lvl = parseInt($('#lvl').val());
    switch (lvl) {
        case 1:
            if (exp >= 300) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 2:
            if (exp >= 900) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 3:
            if (exp >= 2700) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 4:
            if (exp >= 6500) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 5:
            if (exp >= 14000) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 6:
            if (exp >= 23000) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 7:
            if (exp >= 34000) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 8:
            if (exp >= 48000) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 9:
            if (exp >= 64000) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 10:
            if (exp >= 85000) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 11:
            if (exp >= 100000) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 12:
            if (exp >= 120000) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 13:
            if (exp >= 140000) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 14:
            if (exp >= 165000) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 15:
            if (exp >= 195000) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 16:
            if (exp >= 225000) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 17:
            if (exp >= 265000) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 18:
            if (exp >= 305000) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        case 19:
            if (exp >= 355000) {
                $('#lvlUp').prop('disabled', false);
            }
            break;
        default:
            $('#lvlUp').prop('disabled', true);
            break;
    }
}

function levelUp() {
    $('#isSaved').val(true);
    var level = parseInt($('#lvl').val())+1;
    $('#lvl').val(level);
    if (level % 4 === 1) {
        var tmpProf = parseInt($('#profBonus').html().substr(1));
        var symbol = $('#profBonus').html().substr(0, 1);
        tmpProf += 1;
        $('#profBonus').html(symbol + tmpProf);
    }
    else if ((level % 4 === 0 && level !== 20) || $('#lvl').val() === 19){ 
        localStorage.setItem($('#characterId').val()+'setStats'+level, "false");
    }
    $('#lvlUp').prop('disabled', true);
    checkExp();
    updateProficiencies();
    beforeSubmit();
    document.sheet.submit();
}

function updateProficiencies() {    
    var stat1 = $('#stat1').html();
    var stat2 = $('#stat2').html();
    var mod1Changer = parseInt($('#' + stat1 + 'Modifier').html().substr(1));
    var mod2Changer = parseInt($('#' + stat2 + 'Modifier').html().substr(1));
    var sign1 = '+';
    var sign2 = '+';
    if ($('#' + stat1 + 'Modifier').html().substr(0, 1) === '-') {
        mod1Changer = (-1) * mod1Changer;
    }
    if ($('#' + stat2 + 'Modifier').html().substr(0, 1) === '-') {
        mod2Changer = (-1) * mod2Changer;
    }
    var mod1 = mod1Changer + parseInt($('#profBonus').html().substr(1));
    var mod2 = mod2Changer + parseInt($('#profBonus').html().substr(1));
    if (mod1 < 0)
        sign1 = '';
    if (mod2 < 0)
        sign2 = '';
    $('#'+stat1+'Save').html(sign1 + mod1);
    $('#'+stat2+'Save').html(sign2 + mod2);
}

function improveAbilityScore(scores){
    var oneOrMany = 'points';
    $('#str').attr('oldvalue', $('#str').val());
    $('#dex').attr('oldvalue', $('#dex').val());
    $('#con').attr('oldvalue', $('#con').val());
    $('#intl').attr('oldvalue', $('#intl').val());
    $('#wis').attr('oldvalue', $('#wis').val());
    $('#cha').attr('oldvalue', $('#cha').val());
    if(scores === 1)
        oneOrMany = 'point';    
    if(scores > 0)
        alert('You have '+scores+' '+oneOrMany+' to improve your ability scores');
    var counter = 0;
    $('.inner-stat').prop('disabled', false);
    $('.inner-stat').change(function () {
        var tmp = parseInt($(this).attr('oldvalue'));
        tmp += (counter+1);
        if($(this).val() > $(this).attr('oldvalue')){
            $(this).val(tmp);
            counter++;
            availablePoints--;
            tmp = $(this).val();
            $('#availablePoints').val(availablePoints);
        }
        else if($(this).val() === $(this).attr('oldvalue')){
            availablePoints++;
            counter--;
            $('#availablePoints').val(availablePoints);
        }        
        else if($(this).val() < $(this).attr('oldvalue')){ 
            $(this).val($(this).attr('oldvalue'));
        }
        if(counter>=scores && scores >0){
            if(confirm("Is it ok to modify these stats?")){
                $('.inner-stat').prop('disabled', true); 
                beforeSubmit();
                document.getElementById('sheet').submit();
            }
            else{
                $('#str').val($('#str').attr('oldvalue'));
                $('#dex').val($('#dex').attr('oldvalue'));
                $('#con').val($('#con').attr('oldvalue'));
                $('#intl').val($('#intl').attr('oldvalue'));
                $('#wis').val($('#wis').attr('oldvalue'));
                $('#cha').val($('#cha').attr('oldvalue'));
                counter = 0;
            }
        }
        else{
            $('.inner-stat').prop('disabled', false);
        }
    });
}

function firstAbilityScores() {
    $('#newStatsModal').modal('show');
}

function setNewStats() {
    if(validate() === false)
        return;
    if(!confirm("Are you sure you want these initial values?")){
        return;
    }
    var startStats = $('.startStat');
    var hitDie = parseInt($('#totalHitDice').html().substring(2));
    for (var i = 0; i < startStats.length; i++){
        var val = startStats[i].value;
        var name = startStats[i].id.substring(0,3);        
        $('#'+name+'Modifier').html(setMod(val));
        if(name === 'int')
            name = 'intl';
        var prevVal = $('#'+name).val();
        $('#'+name).val(parseInt(val)+parseInt(prevVal));        
        $('#'+name).prop('disabled', true);
    }
    setSaves();
    setHp(hitDie, 0);
    updateProficiencies();
    $('#dexModifier2').html($('#dexModifier').html().substring(1));
    $('#initiative').html($('#dexModifier').html());
    $('#initiativeIn').val($('#dexModifier').html());
    $('#newStatsModal').modal('hide');
}

function validate() {
    var startStats = $('.startStat');
    for (var i = 0; i < startStats.length; i++){
        if(startStats[i].value === 0 || startStats[i].value === ''){
            alert('All stats must be greater than zero');
            return false;
        }
    }
    return true;
}

function setSaves() {
    var mainSaves = document.getElementsByClassName("mainSave");
    for (var i = 0; i < mainSaves.length; i++) {
        var saveName = mainSaves[i].id;
        var stat = saveName.substring(0, 3);
        var modifier = $('#' + stat + 'Modifier').html();
        mainSaves[i].innerHTML = modifier;
    }
    setPassivePerception();
}

function openModalCampaign(){
    $('#newCampaignModal').modal('show');
}

function submitNewCampaign(){
    document.newCampaignForm.submit();
}

function addNewCharacters(){
    document.addCharactersForm.submit();
}

function setArmorClass(){
    $('#dexModifier2').html($('#dexModifier').html().substring(1));
    var dexMod = parseInt($('#dexModifier2').html());
    var armor = parseInt($('#armorValue').html());
    var shield = parseInt($('#shieldValue').html());
    var misc = parseInt($('#miscValue').html());
    $('#armorClass').html(dexMod + armor + shield + misc);
}

function beforeSubmit(){
    enable();
    $('#profnlang').val(removeLineBreaks(document.getElementById('profnlang')));
    $('#inventory').val(removeLineBreaks(document.getElementById('inventory')));
    $('#features').val(removeLineBreaks(document.getElementById('features')));
    $('#attnspells').val(removeLineBreaks(document.getElementById('attnspells')));
}

function removeLineBreaks(item){
    var text = item.value;
    text = text.replace(/(?:\r\n|\r|\n)/g, '%%%');
    text = text.replace(/'/g, '’');
    text = text.replace(/"/g, '\\\"');
    return text;
}

function setPassivePerception(){
    $('#pasWis').html(10+parseInt($('#wisSave').html()));
}