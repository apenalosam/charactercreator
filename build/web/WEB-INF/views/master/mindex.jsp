<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ include file="../header.jsp" %>
<!---------------------------------------------->
<link href="${contextPath}/resources/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="${contextPath}/resources/css/dtable.search.css" rel="stylesheet">
<link href="${contextPath}/resources/css/dataTables.bootstrap.min.css" rel="stylesheet">
<script src="${contextPath}/resources/js/functions.js"></script>
<link href="${contextPath}/resources/css/dtable.search.css" rel="stylesheet">
<link href="${contextPath}/resources/js/plugins/zoomify/zoomify.min.css" rel="stylesheet">
<link href="${contextPath}/resources/js/plugins/toast/jquery.toast.css" rel="stylesheet">
<link href="${contextPath}/resources/js/plugins/jquery.typeahead.min.css" rel="stylesheet">
<script src="${contextPath}/resources/js/plugins/jquery.typeahead.min.js"></script>
<%@ include file="../menu.jsp"%>
<script>
    var path = 'player_sheet'; 
    function newCampaign(){
        path = 'new_campaign';
        setPath(path);
    }
    function loadCampaign(campaignId){
        $('.load'+campaignId).removeClass('fa-check');
        $('.load'+campaignId).addClass('fa-spinner fa-spin');
        setPath('sheet_manager/' + campaignId);
    }
    function setPath(pathId){
        window.location.href='${contextPath}/master/'+pathId;
    }
    function openMasterModal(){
        $("#newInsert").load('${contextPath}/master/campaign_selector');
    }
    function insertCharacters(path){        
        $('.insertCharacters').load('${contextPath}/master/addToCampaign/'+path);
    }
</script>
    <div class="container" style="padding-top: 10%">
        <button id="newcamp" data-target="#newCampaignModal" class="menuButton" onclick='openModalCampaign()'>
            New Campaign
        </button>
        <button id="excamp" data-toggle="modal" data-target="#existingCampaignModal" class="menuButton rightButton" onclick="openMasterModal()">
            Load Campaign
        </button>
    </div>
<!-- Modal -->
<div id="existingCampaignModal" class="modal fade" role="dialog" style="margin-top: 10%;">
    <div class="modal-dialog">
        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="panel-title" class="stylish"><strong>Choose your campaign</strong></h3>
            </div>
            <div class="modal-body" id="modalBody">
                <table id="listOfCharacters" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead class="stylish" style="background-color: #800000; border-color: #800000;">
                        <tr class="btn-primary">
                            <th style="width: 70%;" class="table-header">Name</th>
                            <th style="width: 20%;" class="table-header">Active</th>
                            <th style="width: 10%;" class="table-header">Choose</th>
                        </tr>
                    </thead>
                    <tbody id="newInsert">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="newCampaignModal" class="modal fade" role="dialog" style="margin-top: 10%;">
    <div class="modal-dialog">
        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="panel-title" class="stylish"><strong>New Campaign</strong></h3>
            </div>
            <div class="modal-body" id="newModalBody">
                <form:form action="${contextPath}/master/new_campaign" name="newCampaignForm" id="newCampaignForm" method="POST" enctype="multipart/form-data" accept-charset="UTF-8" autocomplete="off">
                    <div>
                        <label class=" stylish">Campaign name:</label>
                        <div>
                            <input class="stylish" type="text" name="campaignName" id="campaignname">
                        </div>
                    </div>
                    <br/>
                    <div>
                        <label class="stylish">Add users (search by username):</label>
                        <div class="typeahead__container">
                            <div class="typeahead__field">
                                <span class="typeahead__query">
                                    <input class="js-base stylish" id="search_campaign" type="search" placeholder="Search users..." autocomplete="off">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div style="margin-top: 20px;">
                        <table id="selectedCharacters" class="table table-striped table-bordered hide" cellspacing="0" width="100%">
                            <thead class="stylish" style="background-color: #800000; border-color: #800000;">
                                <tr class="btn-primary">
                                    <th style="width: 70%;" class="table-header">Character Name</th>
                                    <th style="width: 20%;" class="table-header">Username</th>
                                </tr>
                            </thead>
                            <tbody id="insertCharacters">
                            </tbody>
                        </table>
                        <input type="hidden" name="characterList" id="characterList">
                    </div>
                </form:form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-default" onclick="submitNewCampaign()">Save with these characters</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var characterIds = [];
        $.typeahead({
            input: '.js-base',
            minLength: 1,
            maxItem: 15, //hasta 15 registro muestra
            hint: true,
            cache: false,
            template: '<div class="ProfileCard u-cf">'+
                      '<table width="100%">'+
                      '<tr>'+
                      '<td width="15%"><strong><i>Username:</i></strong>&nbsp</td>'+
                      '<td width="35%">{{userName}}</td>'+
                      '<td width="15%"><strong><i>Character Name:</i><strong>&nbsp</td>'+
                      '<td width="35%">{{characterName}}</td>'+
                      '</tr>'+
                      '</table>'+
                      '</div>',
            emptyTemplate: '<div class="MensajeVacio">The user/character {{query}} doesn\'t exist<br>Please check your spelling and make sure that the character has been created</div>',
            dynamic: true,
            source: {
                base: {
                    display: ["userName", "characterName"],
                    ajax: {
                        url: "${contextPath}/master/listOfCharacters",
                        path: "data",
                        data: {
                            q: "{{query}}"
                        }
                    }
                }
            },
            callback: {
                onNavigateBefore: function (node, query, event) {
                    if (~[38, 40].indexOf(event.keyCode)) {
                        event.preventInputChange = true;
                    }
                },
                onClick: function (node, a, item, event) {
                    var html = $('#insertCharacters').html();
                    if(characterIds.includes(item.characterId)===false){
                        characterIds.push(item.characterId);
                        html += '<tr><td>'+item.characterName+'</td>';
                        html += '<td>'+item.userName+'</td></tr>';
                    }
                    $('#insertCharacters').html(html);
                    var chLen = characterIds.length;
                    var charIds = characterIds[0];
                    for (var i=1; i<chLen; i++){
                        charIds += ","+characterIds[i];
                    }
                    $('#characterList').val(charIds);
                    $('#selectedCharacters').removeClass('hide');
                }
            }
        });
    });
</script>
<%@ include file="../footer.jsp" %>