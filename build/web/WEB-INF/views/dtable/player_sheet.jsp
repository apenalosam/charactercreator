<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ include file="../header.jsp"%>
<%@ include file="../menu.jsp"%>
<link href="${contextPath}/resources/js/plugins/jquery.typeahead.min.css" rel="stylesheet">
<link href="${contextPath}/resources/js/plugins/toast/jquery.toast.css" rel="stylesheet">
<script>
    var isNew;
    var isMaster;
    var availablePoints;
    $(document).ready(function () {
        isNew = ${isNew};
        if (${c.alive}===false && isNew===false) {
            document.getElementById('blanket').className = document.getElementById('blanket').className.replace("hide", "show");
            document.getElementById('deathMsg').className = document.getElementById('deathMsg').className.replace("hide", "show");
        } else {
            document.getElementById('blanket').className = document.getElementById('blanket').className.replace("show", "hide");
            document.getElementById('deathMsg').className = document.getElementById('deathMsg').className.replace("show", "hide");
        }
        isMaster = ${isMaster};
        availablePoints = ${c.availablePoints};
        if (!isNew) {
            $('#armorValue').html('${c.armorValue}');
            $('#shieldValue').html('${c.shieldValue}');
            $('#miscValue').html('${c.miscValue}');
            $('#dexModifier2').html($('dexModifier').html().substring(1));
            setArmorClass();
        }        
        if ('${message}' !== "") {
            alert('${message}');
        }
        if (!isNew) {
        
    }
    if(availablePoints > 0)
        improveAbilityScore(availablePoints);
    });

</script>
<!-- Modal -->
<div id="newStatsModal" class="modal fade" role="dialog" style="margin-top: 8%;">
    <div class="modal-dialog">
        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="panel-title stylish"><strong>Assign your stats' values</strong></h3>
            </div>
            <div class="modal-body" id="statsModalBody">
                <label class="stylish" style="margin-left: 8%; float: left;">Strength<input class="startStat stylish" type="number" min="1" max="20" id="strStart" style="width:60px; margin-left: 30px;"></label>
                <label class="stylish" style="margin-left: 30px; float: right"><input class="startStat stylish" type="number" min="1" max="20" id="dexStart" style="width:60px; margin-right:30px;">Dexterity</label>
                <br/>
                <label class="stylish" style="margin-left: 8%; float: left;">Constitution<input class="startStat stylish" type="number" min="1" max="20" id="conStart" style="width:60px; margin-left: 30px;"></label>
                <label class="stylish" style="margin-left: 30px; float: right"><input class="startStat stylish" type="number" min="1" max="20" id="intStart" style="width:60px; margin-right:30px;">Intelligence</label>
                <br/>
                <label class="stylish" style="margin-left: 8%; float: left;">Wisdom<input class="startStat stylish" type="number" min="1" max="20" id="wisStart" style="width:60px; margin-left: 30px;"></label>
                <label class="stylish" style="margin-left: 30px; float: right"><input class="startStat stylish" type="number" min="1" max="20" id="chaStart" style="width:60px; margin-right:30px;">Charisma</label>
                <br/>
                <br/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="setNewStats()">Set</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="hidden" id="loader"></div>
<div class="hide disabler" id="blanket"></div>
<div class="dead-message hide stylish" id="deathMsg" style="background-color: transparent;">
    <label id="dthMsgLbl" class="stylish" style="color: red; font-size: 80px; text-align: center; width: 100%">YOU ARE DEAD</label>
    <button id="revive" class="stylish hide" onclick="changeAlive()" style="margin-left: 0px;">Revive</button>
</div>
<div id="mainDiv" class='div-bckImg' style="background-image: url('${contextPath}/resources/img/char_sheet.png'); position: static; height: 1740px">
    <form:form action="${contextPath}/dtable/save_sheet" name="sheet" id="sheet" method="POST" enctype="multipart/form-data" accept-charset="UTF-8" autocomplete="off"> 
        <!--<input id="characterName" name="characterName" type="text" class="char-name stylish text-center" value="${c.characterName}">-->
        <div class="form-group">
            <div class="input-group">
                <input name="characterName" spellcheck="false" id="characterName" type="text" class="char-name form-control stylish" style="width:380px" value="${c.characterName}"/>

                <input name="characterId" id="characterId" type="hidden" value="${c.characterId}">
                <input name="armorValueIn" id="armorValueIn" type="hidden" value="0">
                <input name="shieldValueIn" id="shieldValueIn" type="hidden" value="0">
                <input name="miscValueIn" id="miscValueIn" type="hidden" value="0">
                <input name="initiativeIn" id="initiativeIn" type="hidden" value="0">
                <input name="inspBonusIn" id="inspBonusIn" type="hidden" value="0">

                <input name="availablePoints" id="availablePoints" type="hidden" value="${c.availablePoints}">
                <input name="lastLevelSet" id="lastLevelSet" type="hidden" value="${c.lastLevelSet}">

                <input name="campaignId" spellcheck="false" id="campaignId" type="hidden" class="form-control char-id stylish" style="width:380px" value="${c.campaignId}"/>

                <input name="playerName" spellcheck="false" id="playerName" type="text" class=" player-name form-control stylish" style="width:200px" value="${user.names} ${user.lastNames}"/>

                <select name="classId" id="classId" type="text" class="class-select form-control stylish" style="width:150px;">
                    <option value="-1" disabled selected >Select your class</option>
                    <c:forEach var="clas" items="${classes}">
                        <option value="${clas.classId}">${clas.clasName}</option>
                    </c:forEach>
                </select>

                <input style="width:60px;" name="lvl" id="lvl" min="1" max="20" type="number" class="class-lvl form-control stylish"/>

                <select name="backgroundId" id="backgroundId" type="text" class="background-select form-control stylish" style="width:205px;">
                    <option value="-1" disabled selected >-Select your background-</option>
                    <c:forEach var="background" items="${backgrounds}">
                        <option value="${background.id}">${background.name}</option>
                    </c:forEach>
                </select>

                <select style="width:220px;" name="raceId" id="raceId" type="text" class="race-select form-control stylish">
                    <option value="-1" disabled selected>-Select your race-</option>
                    <c:forEach var="race" items="${races}">
                        <option value="${race.raceId}">${race.raceName}</option>
                    </c:forEach>
                </select>

                <select style="width:210px;" name="alignId" id="alignId" type="text" class="align-select form-control stylish">
                    <option value="-1" disabled selected>-Select your alignment-</option>
                    <c:forEach var="align" items="${alignments}">
                        <option value="${align.alignmentId}">${align.alignName}</option>
                    </c:forEach>
                </select>

                <input style="width:185px" name="expPoints" id="expPoints" type="number" style="text-align: center;" class="exp-points form-control stylish" value="${c.expPoints}"/>
            </div> 
            <div class="lvl-up">
                <button id="lvlUp" style="text-align: center;" class="btn-primary stylish" onclick="levelUp()">Level up</button>
                <input type="hidden" id="isSaved" name="isSaved" value="false">
            </div>
            <div style="width:100px; position:static">
                <label id="profBonus" class="stylish prof-bonus">+2</label>
                <br/>
                <label id="inspBonus" class="stylish insp-bonus defValue" contenteditable="true">0</label>
            </div>
            <div class="third-pane">                
                <label id="armorClass" class="stylish armor-class defValue">0</label>
                <label id="dexModifier2" class="stylish dex-modifier defValue">0</label>
                <label id="armorValue" class="stylish armor-value defValue" contenteditable="true">0</label>
                <label id="shieldValue" class="stylish shield-value defValue" contenteditable="true">0</label>
                <label id="miscValue" class="stylish misc-value defValue" contenteditable="true">0</label>
                <label id="initiative" class="stylish initiative defValue" contenteditable="true">0</label>
                <div style="float: right; margin-top: -10px;">
                    <label tooltip="Regular speed" class="stylish" style="width: 30px; font-size: 20px; margin-left: 30px">R /</label>
                    <label tooltip="Speed while wearing heavy armor" class="stylish" style="width: 30px; font-size: 20px;">H /</label>
                    <label tooltip="Swimming speed" class="stylish" style="width: 30px; font-size: 20px;">S</label>
                    <br/>
                    <label id="speed" class="stylish speed"></label>                       
                </div>
            </div>            

            <div class="input-group fourth-pane" style="width: 87px;">
                <input name="str" id="str" type="number" class="form-control no-border text-center stylish inner-stat" value="${c.str}" oldvalue="0"/>
                <label id="strModifier" class="no-border text-center stylish stat-modifier">+0</label>            
                <input name="dex" id="dex" type="number" class="form-control no-border text-center stylish inner-stat" value="${c.dex}" oldvalue="0"/>                
                <label id="dexModifier" class="no-border text-center stylish stat-modifier">+0</label>                
                <input name="con" id="con" type="number" class="form-control no-border text-center stylish inner-stat" value="${c.con}" oldvalue="0"/>                
                <label id="conModifier" class="no-border text-center stylish stat-modifier">+0</label>                
                <input name="intl" id="intl" type="number" class="form-control no-border text-center stylish inner-stat" value="${c.intl}" oldvalue="0"/>
                <label id="intModifier" class="no-border text-center stylish stat-modifier">+0</label>
                <input name="wis" id="wis" type="number" class="form-control no-border text-center stylish inner-stat" value="${c.wis}" oldvalue="0"/>
                <label id="wisModifier" class="no-border text-center stylish stat-modifier">+0</label>
                <input name="cha" id="cha" type="number" class="form-control no-border text-center stylish inner-stat" value="${c.cha}" oldvalue="0"/>
                <label id="chaModifier" class="no-border text-center stylish stat-modifier">+0</label>
                <br/>
                <label id="pasWis" class="passive-wisdom stylish no-border text-center">0</label>
                <textarea spellcheck="false" class="stylish items no-border" name="profnlang" id="profnlang" style="resize: none"></textarea>
            </div>
            <div class="input-group fifth-pane" style="min-width: 0px; width: 20px;">
                <canvas id="strDiamond" style="margin-left:3px; background-color: transparent;border:1px solid #000000;transform: rotate(45deg); width:12px; height: 12px;"></canvas>
                <br/>
                <input id="athstr" name="athstr" value="athstr" type="checkbox" class="chkbx form-control form-check-input" onchange="chkBx(this)" style="width:18px; height: 18px; "/>
                <br/>
                <canvas id="dexDiamond" class="diamond" style="margin-top: 115px;"></canvas>
                <br/>
                <input id="acrdex" value="acrdex" name="acrdex" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px; "/>
                <br/>
                <input id="sledex" value="sledex" name="sledex" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px; margin-top: 2px"/>
                <br/>
                <input id="stedex" value="stedex" name="stedex" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px; margin-top: 2.5px"/>
                <br/>
                <canvas id="conDiamond" class="diamond" style="margin-top: 75px;"></canvas>
                <br/>
                <canvas id="intDiamond" class="diamond-typetwo" style="margin-top: 135px;"></canvas>
                <br/>
                <input id="arcint" value="arcint" name="arcint" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px;  margin-top: 2.5px"/>
                <br/>
                <input id="hisint" value="hisint" name="hisint" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px; margin-top: 2.5px"/>
                <br/>
                <input id="invint" value="invint" name="invint" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px; margin-top: 2.5px"/>
                <br/>
                <input id="natint" value="natint" name="natint" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px;  margin-top: 3px"/>
                <br/>
                <input id="relint" value="relint" name="relint" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px; margin-top: 3px"/>
                <br/>
                <canvas id="wisDiamond" class="diamond" style="margin-top: 34px;"></canvas>
                <br/>
                <input id="aniwis" value="aniwis" name="aniwis" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px;  margin-top: 2.5px"/>
                <br/>
                <input id="inswis" value="inswis" name="inswis" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px; margin-top: 3px"/>
                <br/>
                <input id="medwis" value="medwis" name="medwis" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px; margin-top: 3px"/>
                <br/>
                <input id="perwis" value="perwis" name="perwis" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px;  margin-top: 3px"/>
                <br/>
                <input id="surwis" value="surwis" name="surwis" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px; margin-top: 3px"/>
                <br/>
                <canvas id="chaDiamond" class="diamond" style="margin-top: 34px;"></canvas>
                <br/>
                <input id="deccha" value="deccha" name="deccha" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px;  margin-top: 3px"/>
                <br/>
                <input id="inicha" value="inicha" name="inicha" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px; margin-top: 3px"/>
                <br/>
                <input id="pefcha" value="pefcha" name="pefcha" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px; margin-top: 3px"/>
                <br/>
                <input id="pescha" value="pescha" name="pescha" type="checkbox" class="chkbx form-control" onchange="chkBx(this)" style="width:18px; height: 18px;  margin-top: 3px"/>
            </div>
            <div class="input-group sixth-pane" style="width: 198px">
                <label class="saveLbl mainSave stylish" id="strSave">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="athSave">+0</label><br/>
                <label class="saveLbl mainSave stylish" id="dexSave" style="margin-top: 106px;">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="acrSave">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="sleSave">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="steSave">+0</label><br/>
                <label class="saveLbl mainSave stylish" id="conSave" style="margin-top: 62px;">+0</label><br/>
                <label class="saveLbl mainSave stylish" id="intSave" style="margin-top: 126px;">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="arcSave">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="hisSave">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="invSave">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="natSave">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="relSave">+0</label><br/>
                <label class="saveLbl mainSave stylish" id="wisSave" style="margin-top: 24px;">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="aniSave">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="insSave">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="medSave">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="perSave">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="surSave">+0</label><br/>
                <label class="saveLbl mainSave stylish" id="chaSave" style="margin-top: 22px;">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="decSave">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="iniSave">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="pefSave">+0</label><br/>
                <label class="saveLbl stylish mid-lbl" id="pesSave">+0</label><br/>
            </div>  
            <div class="input-group seventh-pane" style="width:430px">
                <label name="maxHpLbl" id="maxHpLbl" class="max-hp stylish">0</label>
                <input type="hidden" name="maxHp" id="maxHp">
                <input type="hidden" name="alive" id="alive" value="1">
                <br/>
                <input name="currentHp" id="currentHp" type="number" style="width:300px;" class="form-control text-center stylish current-hp" value="${c.currentHp}"/>
                <br/>
                <div style="width:430px; margin-left:40px">
                    <div style="width:100%">
                        <label id="totalHitDice" class="stylish text-center total-hit-dice">0</label>
                        <br/>
                        <label id="hitDice" class="stylish text-center hit-dice">0</label>
                        <div style="margin-top: -78px; margin-left: 275px;" class="input-group">
                            <input type="radio" class="rad-button" name="successes" id="success1">
                            <input type="radio" class="rad-button" name="successes" id="success2" style="margin-left: 5px;">
                            <input type="radio" class="rad-button" name="successes" id="success3" style="margin-left: 6px;">
                        </div>
                        <div style="margin-top: 6px; margin-left: 275px;" class="input-group">
                            <input type="radio" class="rad-button" name="failures" id="failure1" value="1">
                            <input type="radio" class="rad-button" name="failures" id="failure2" value="2" style="margin-left: 5px;">
                            <input type="radio" class="rad-button" name="failures" id="failure3" value="3" style="margin-left: 6px;">
                        </div>
                    </div>
                    <div>
                        <div>
                            <input type="text" name="weapon1" id="weapon1" spellcheck="false" class="weapons no-border stylish" style="margin-top: 108px;">
                            <input type="text" name="atkBns1" id="atkBns1" spellcheck="false" class="atkBns no-border stylish">
                            <input type="text" name="dmgType1" id="dmgType1" spellcheck="false" class="dmgType no-border stylish">
                        </div>
                        <div>
                            <input type="text" name="weapon2" id="weapon2" spellcheck="false" class="weapons no-border stylish" style="margin-top: 12px;">
                            <input type="text" name="atkBns2" id="atkBns2" spellcheck="false" class="atkBns no-border stylish">
                            <input type="text" name="dmgType2" id="dmgType2" spellcheck="false" class="dmgType no-border stylish">
                        </div>
                        <div>
                            <input type="text" name="weapon3" id="weapon3" spellcheck="false" class="weapons no-border stylish" style="margin-top: 12px;">
                            <input type="text" name="atkBns3" id="atkBns3" spellcheck="false" class="atkBns no-border stylish">
                            <input type="text" name="dmgType3" id="dmgType3" spellcheck="false" class="dmgType no-border stylish">
                        </div>
                        <div>
                            <textarea spellcheck="false" name="attnspells" id="attnspells" class="atk-and-spells stylish no-border" style="resize: none"></textarea>
                        </div>
                    </div>
                </div>
                <div style="margin-top: 55px;">
                    <div style="width: 75px; margin-left: 60px;">
                        <input style="width:70px" name="cp" id="cp" type="number" class="currency form-control no-border text-center stylish" value="${c.cp}"/>
                        <br/>
                        <input style="width:70px" name="sp" id="sp" type="number" class="currency form-control no-border text-center stylish" value="${c.sp}"/>
                        <br/>
                        <input style="width:70px" name="gp" id="gp" type="number" class="currency form-control no-border text-center stylish" value="${c.gp}"/>
                        <br/>
                        <input style="width:70px" name="pp" id="pp" type="number" class="currency form-control no-border text-center stylish" value="${c.pp}"/>                        
                    </div>
                    <div class="equipmentBox">
                        <textarea spellcheck="false" name="inventory" id="inventory" class="equipment form-control stylish no-border" style="height:295px; line-height: 24px; resize: none"></textarea>                        
                    </div>
                </div>
            </div>
            <div class="input-group eigth-pane" style="width:430px">
                <div>
                    <div>
                        <textarea spellcheck="false" name="traits" id="traits" class="form-control stylish no-border" style="width:340px; height:85px; resize: none"></textarea>
                    </div>
                    <div>
                        <textarea spellcheck="false" name="ideals" id="ideals" class="form-control stylish no-border" style="width:340px; margin-top: 31px; height:62px; resize: none"></textarea>
                    </div>
                    <div>
                        <textarea spellcheck="false" name="bonds" id="bonds" class="form-control stylish no-border" style="width:340px; margin-top: 33px; height:62px; resize: none"></textarea>
                    </div>
                    <div>
                        <textarea spellcheck="false" name="flaws" id="flaws" class="form-control stylish no-border" style="width:340px; margin-top: 31px; height:62px; resize: none"></textarea>
                    </div>
                </div>
                <div style="margin-left: -22px;">
                    <textarea spellcheck="false" name="features" id="features" class="form-control stylish no-border" style="font-size: 16px; width: 386px; height: 833px; margin-top: 52px; line-height: 0.6614cm; resize: none"></textarea>
                </div>
            </div>
        </div>
        <div class="form-group" >
            <button id="regButton" class="bottom-right-button stylish" type="submit" onclick="beforeSubmit()" style="font-size: 30px; position: fixed; bottom: 5px; right: 5px; z-index: 20">${btnName}</button>
        </div> 
        <div name="raceCharacs" class="hidden">
            <label id="stat1"></label>
            <label id="stat2"></label>
            <label id="raceStr"></label>
            <label id="raceDex"></label>
            <label id="raceCon"></label>
            <label id="raceInt"></label>
            <label id="raceWis"></label>
            <label id="raceCha"></label>
            <label id="adulthood"></label>
            <label id="avgAge"></label>
            <label id="typicalAlignment"></label>
            <label id="size"></label>
        </div>
    </form:form>
</div>
<script src="${contextPath}/resources/js/functions.js"></script>
<script>
    
                formData = new FormData($(this[0]));
                function setCharacter() {
                    $('#playerName').text('${c.playerName}');
                    $('#playerName').val('${c.playerName}');
                    $('#characterId').val('${c.characterId}');
                    $('#characterName').val('${c.characterName}');
                    $('#lvl').val('${c.lvl}');
                    $('#classId').val('${c.classId}');
                    $('#profBonus').html('+${profBonus}');
                    $('#strModifier').html(setMod(${c.str}));
                    $('#dexModifier').html(setMod(${c.dex}));
                    $('#initiativeIn').val(parseInt($('#dexModifier').html()));
                    $('#conModifier').html(setMod(${c.con}));
                    $('#intModifier').html(setMod(${c.intl}));
                    $('#wisModifier').html(setMod(${c.wis}));
                    $('#chaModifier').html(setMod(${c.cha}));
                    $('#inspBonus').html('${c.inspBonus}');
                    setCheckBoxes();
                    setPassivePerception();
                    $('#raceId').val('${c.raceId}');
                    $('#alignId').val('${c.alignId}');
                    $('#armorValue').html(${c.armorValue});
                    $('#armorValueIn').html(${c.armorValue});
                    $('#shieldValue').html(${c.shieldValue});
                    $('#shieldValueIn').html(${c.shieldValue});
                    $('#miscValue').html(${c.miscValue});
                    $('#miscValueIn').val(${c.miscValue});
                    $('#initiative').html($('#dexModifier').html());
                    $('#traits').val('${c.traits}');
                    $('#bonds').val('${c.bonds}');
                    $('#ideals').val('${c.ideals}');
                    $('#flaws').val('${c.flaws}');
                    var text = '${c.profiAndLang}';
                    text = text.replace(/%%%/g, '\n');
                    $('#profnlang').val(text);
                    text = '${c.inventory}';
                    text = text.replace(/%%%/g, '\n');
                    $('#inventory').val(text);
                    text = '${c.feats}';
                    text = text.replace(/%%%/g, '\n');
                    $('#features').val(text);
                    text = '${c.attacksAndSpells}';
                    text = text.replace(/%%%/g, '\n');
                    $('#attnspells').val(text);
                    setArmorClass();
                    getRace(${c.raceId});
                    getClas(${c.classId});
                    setRace();
                    setBackground();
                    setWeapons();
                }
                function setWeapons(){
                    var weapon1 = '${c.weaponOne}'.split("&;");
                    weapon1.push("1");
                    var weapon2 = '${c.weaponTwo}'.split("&;");
                    weapon2.push("2");
                    var weapon3 = '${c.weaponThree}'.split("&;");
                    weapon3.push("3");
                    var weapons = [weapon1, weapon2, weapon3];
                    weapons.forEach(weapon =>{
                        if(weapon.length > 0){
                            var num = weapon[weapon.length-1];
                            $('#weapon'+num).val(weapon[0]);
                            $('#atkBns'+num).val(weapon[1]);
                            $('#dmgType'+num).val(weapon[2]);
                        }                        
                    });
                }
                function setCheckBoxes() {
                    var saving = '${c.checkboxes}'.split('&;');
                    if(saving.length !== null){
                        saving.forEach(save => {
                            $('#' + save).prop('checked', true);
                            var cb = document.getElementById(save);
                            chkBx(cb);
                        });
                    }
                }
                function setSpeed() {
                    var speeds = '';
                    if ('${r.landSpeed}' !== "") {
                        speeds += '${r.landSpeed}/';
                    } else {
                        speeds += '0/';
                    }
                    if ('${r.armorSpeed}' !== "") {
                        speeds += '${r.armorSpeed}/';
                    } else {
                        speeds += '0/';
                    }
                    if ('${r.landSpeed}' !== "") {
                        speeds += '${r.swimSpeed}';
                    } else {
                        speeds += '0';
                    }
                    $('#speed').html(speeds);
                }
                function setClas() {
                    setSaves();
                    var saves = '${cl.saves}'.split("&;");
                    var stat1 = saves[0].substr(0, 3);
                    var stat2 = saves[1].substr(0, 3);
                    $('#stat1').html(stat1);
                    $('#stat2').html(stat2);
                    var mod1Changer = parseInt($('#' + stat1 + 'Modifier').html());
                    var mod2Changer = parseInt($('#' + stat2 + 'Modifier').html());
                    if ($('#' + stat1 + 'Modifier').html().substr(0, 1) === '-') {
                        mod1Changer = (-1) * mod1Changer;
                    }
                    if ($('#' + stat2 + 'Modifier').html().substr(0, 1) === '-') {
                        mod2Changer = (-1) * mod2Changer;
                    }
                    var mod1 = mod1Changer + parseInt($('#profBonus').html().substr(1));
                    var mod2 = mod2Changer + parseInt($('#profBonus').html().substr(1));
                    $('#' + saves[0]).html('+' + mod1);
                    $('#' + saves[1]).html('+' + mod2);
                    $('#' + stat1 + 'Diamond').css("background-color", "#000000");
                    $('#' + stat2 + 'Diamond').css("background-color", "#000000");
                    $('#maxHpLbl').html('${cl.hitPoints + (cl.hitPointsOptional*(c.lvl-1))}');
                    $('#hitDice').html($('#lvl').val());
                    $('#totalHitDice').html($('#lvl').val() + 'd${cl.hitDie}');
                }
                function setRace() {
                    $('#raceStr').html('${r.baseStr}');
                    $('#raceDex').html('${r.baseDex}');
                    $('#raceCon').html('${r.baseCon}');
                    $('#raceInt').html('${r.baseInt}');
                    $('#raceWis').html('${r.baseWis}');
                    $('#raceCha').html('${r.baseCha}');
                    $('#adulthood').html('${r.adulthood}');
                    $('#avgAge').html('${r.avgAge}');
                    $('#typicalAlignment').html('${r.typicalAlignment}');
                    $('#size').html('${r.size}');
                    $('#strModifier').html(setMod(${r.baseStr}));
                    $('#dexModifier').html(setMod(${r.baseDex}));
                    $('#conModifier').html(setMod(${r.baseCon}));
                    $('#intModifier').html(setMod(${r.baseInt}));
                    $('#wisModifier').html(setMod(${r.baseWis}));
                    $('#chaModifier').html(setMod(${r.baseCha}));
                    setSpeed();
                }
                function setBackground() {
                    $('#backgroundId').val('${c.backgroundId}');
                }
                function getRace(selection) {
                    $.ajax({
                        url: '${contextPath}/dtable/getRace/' + selection,
                        type: 'POST',
                        cache: false,
                        success: function (response) {
                            var speeds = response.landSpeed + "/" + response.armorSpeed + "/" + response.swimSpeed;
                            if (addedRaceStats) {
                                $('#str').val(parseInt($('#str').val()) - parseInt($('#raceStr').html()));
                                $('#dex').val(parseInt($('#dex').val()) - parseInt($('#raceDex').html()));
                                $('#con').val(parseInt($('#con').val()) - parseInt($('#raceCon').html()));
                                $('#intl').val(parseInt($('#intl').val()) - parseInt($('#raceInt').html()));
                                $('#wis').val(parseInt($('#wis').val()) - parseInt($('#raceWis').html()));
                                $('#cha').val(parseInt($('#cha').val()) - parseInt($('#raceCha').html()));
                            }
                            $('#str').val(parseInt($('#str').val()) + parseInt(response.baseStr));
                            $('#dex').val(parseInt($('#dex').val()) + parseInt(response.baseDex));
                            $('#con').val(parseInt($('#con').val()) + parseInt(response.baseCon));
                            $('#intl').val(parseInt($('#intl').val()) + parseInt(response.baseInt));
                            $('#wis').val(parseInt($('#wis').val()) + parseInt(response.baseWis));
                            $('#cha').val(parseInt($('#cha').val()) + parseInt(response.baseCha));
                            $('#raceStr').html(response.baseStr);
                            $('#raceDex').html(response.baseDex);
                            $('#raceCon').html(response.baseCon);
                            $('#raceInt').html(response.baseInt);
                            $('#raceWis').html(response.baseWis);
                            $('#raceCha').html(response.baseCha);
                            $('#adulthood').html(response.adulthood);
                            $('#avgAge').html(response.avgAge);
                            $('#typicalAlignment').html(response.typicalAlignment);
                            $('#size').html(response.size);
                            $('#speed').html(speeds);
                            $('#strModifier').html(setMod($('#str').val()));
                            $('#dexModifier').html(setMod($('#dex').val()));
                            $('#conModifier').html(setMod($('#con').val()));
                            $('#intModifier').html(setMod($('#intl').val()));
                            $('#wisModifier').html(setMod($('#wis').val()));
                            $('#chaModifier').html(setMod($('#cha').val()));
                            addedRaceStats = true;
                        },
                        error: function (jqXhr, textStatus, errorThrown) {
                            alert(jqXhr);
                            alert(textStatus);
                            alert(errorThrown);
                        }
                    });
                }
                function getClas(selection) {
                    $.ajax({
                        url: '${contextPath}/dtable/getClas/' + selection,
                        type: 'POST',
                        cache: false,
                        success: function (response) {
                            setHp(response.hitPoints, response.hitPointsOptional);
                            var saves = response.saves.split("&;");
                            var stat1 = saves[0].substr(0, 3);
                            var stat2 = saves[1].substr(0, 3);
                            $('#stat1').html(stat1);
                            $('#stat2').html(stat2);
                            if (addedClasStats && isNew) {
                                $('#strSave').html('+0');
                                $('#dexSave').html('+0');
                                $('#conSave').html('+0');
                                $('#intSave').html('+0');
                                $('#wisSave').html('+0');
                                $('#chaSave').html('+0');
                            }
                            setSaves();
                            var mod1Changer = parseInt($('#' + stat1 + 'Modifier').html().substr(1));
                            var mod2Changer = parseInt($('#' + stat2 + 'Modifier').html().substr(1));
                            var sign1 = '+';
                            var sign2 = '+';
                            if ($('#' + stat1 + 'Modifier').html().substr(0, 1) === '-') {
                                mod1Changer = (-1) * mod1Changer;
                            }
                            if ($('#' + stat2 + 'Modifier').html().substr(0, 1) === '-') {
                                mod2Changer = (-1) * mod2Changer;
                            }
                            var mod1 = mod1Changer + parseInt($('#profBonus').html().substr(1));
                            var mod2 = mod2Changer + parseInt($('#profBonus').html().substr(1));
                            if (mod1 < 0)
                                sign1 = '';
                            if (mod2 < 0)
                                sign2 = '';
                            $('#' + saves[0]).html(sign1 + mod1);
                            $('#' + saves[1]).html(sign2 + mod2);
                            $('#strDiamond').css("background-color", "transparent");
                            $('#dexDiamond').css("background-color", "transparent");
                            $('#conDiamond').css("background-color", "transparent");
                            $('#intDiamond').css("background-color", "transparent");
                            $('#wisDiamond').css("background-color", "transparent");
                            $('#chaDiamond').css("background-color", "transparent");
                            $('#' + stat1 + 'Diamond').css("background-color", "#000000");
                            $('#' + stat2 + 'Diamond').css("background-color", "#000000");
                            $('#hitDice').html($('#lvl').val());
                            $('#totalHitDice').html($('#lvl').val() + 'd' + response.hitDie);
                            addedClasStats = true;
                        },
                        error: function (jqXhr, textStatus, errorThrown) {
                            alert(jqXhr);
                            alert(textStatus);
                            alert(errorThrown);
                        }
                    });
                }
                function getAlign(selection) {
                    $.ajax({
                        url: '${contextPath}/dtable/getAlign/' + selection,
                        type: 'POST',
                        cache: false,
                        success: function (response) {
                        },
                        error: function (jqXhr, textStatus, errorThrown) {
                            alert(jqXhr);
                            alert(textStatus);
                            alert(errorThrown);
                        }
                    });
                }
                function changeAlive() {
                    $.ajax({
                        url: '${contextPath}/dtable/change_alive/' + $('#characterId').val(),
                        type: 'POST',
                        cache: false,
                        success: function (response) {
                            location.reload();
                        },
                        error: function (jqXhr, textStatus, errorThrown) {
                            alert(jqXhr);
                            alert(textStatus);
                            alert(errorThrown);
                        }
                    });
                }
                function checkAlive() {
                    $('#alive').val(${c.alive});
                }
                function chkBx(item) {
                    var saves = '${cl.saves}'.split("&;");
                    var stat1 = saves[0].substr(0, 3);
                    var stat2 = saves[1].substr(0, 3);
                    var lblName = item.id.substring(0, 3);
                    var cat = item.id.substring(3);
                    var val = parseInt($('#' + cat + 'Modifier').html());
                    if (stat1===cat || stat2===cat){
                        val = parseInt(val) + parseInt($('#profBonus').html());
                    }
                    var sign = '+';
                    if (val < 0)
                        sign = '';
                    lblName += 'Save';
                    if ($(item).is(':checked')) {
                        $('#' + lblName).html(sign + val);
                    } else {
                        $('#' + lblName).html('+0');
                    }
                }
</script>
<%@ include file="../footer.jsp" %>
